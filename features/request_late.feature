Feature: Late Requests for Teaching Assistant
	In order to manage requests for teaching assistante
	As an Admin
	I don't want normal professors to be able to make new requests for closed semesters
	And I want super and hiper professors to be able to make new requests for closed semesters

	Background:
		And there is a department with code "MAC"
        And there is a department with code "MAE"
		And there is a professor with name "Bob" and password "prof-123" nusp "12333" department "MAC" and email "bob@bob.bob"
        And there is a super_professor with name "Mandel" and password "prof-123" nusp "12344" department "MAC" and email "kira@bob.bob"
        And there is a hiper_professor with name "Claudia" and password "prof-123" nusp "12355" department "MAE" and email "claudia@ime.br"
		And there is a course with name "Mascarenhas" and code "MAC0300" and department "MAC"

    Scenario: Normal professor should see closed semester
    	Given there is a closed semester "2014" "1"
    	And I'm logged in as professor "Bob"
        And I should see "Pedidos de monitoria"
        Then I click the "Pedidos de monitoria" link
        And I should not see "Pedir monitor(es)"

    Scenario: Super professor should see closed semester
    	Given there is a closed semester "2014" "1"
    	And I'm logged in as professor "Mandel"
        And I should see "Pedidos de monitoria"
        Then I click the "Pedidos de monitoria" link
        And I should not see "Pedir monitor(es)"

	Scenario: Hiper professor should see closed semester
    	Given there is a closed semester "2014" "1"
        And I'm logged in as professor "Claudia"
        And I should see "Pedidos de monitoria"
        Then I click the "Pedidos de monitoria" link
        And I should not see "Pedir monitor(es)"

    Scenario: Super professor should request a assistant
        Given there is a closed semester "2014" "1"
        And I'm logged in as professor "Mandel"
        And I should see "Controle de pedidos"
        Then I click the "Controle de pedidos" link
        And I should see "Pedidos do 1º semestre de 2014"

	Scenario: Normal professor should not be able to access teaching request page when semester is closed but active
		Given there is a closed but active semester "2014" "1"
    	And I'm logged in as professor "Bob"
        When I go to the new request form
        Then I should not see "Novo Pedido por Monitor"

	Scenario: Super professor should request an assistant when the period is closed
		Given there is a closed but active semester "2014" "1"
        And I'm logged in as professor "Mandel"
        And I should see "Controle de pedidos"
        Then I click the "Controle de pedidos" link
        And I should see "Pedir monitor(es) para um(a) professor(a)"
		Then I click the "Pedir monitor(es) para um(a) professor(a)" link
        And I select "MAC0300 - Mascarenhas" on the "Disciplina"
        And I fill the "Número de monitores solicitados" field with "2"
        And I select the priority option "Extremamente necessário, mas não imprescindível"
        And I mark the "Correção de trabalhos" checkbox
        And I mark the "Fiscalização de provas" checkbox
        And I write on the "Observações" text area "teste observações"
        And I press the "Enviar" button
        Then I should see "Pedido de Monitoria feito com sucesso"
        And I should see "Disciplina: MAC0300 - Mascarenhas"
        And I should see "Número de monitores solicitados: 2"
        And I should see "Prioridade: Extremamente necessário, mas não imprescindível"
        And I should see "Atendimento aos alunos: Não"
        And I should see "Correção de trabalhos: Sim"
        And I should see "Fiscalização de provas: Sim"
        And I should see "Observações: teste observações"
