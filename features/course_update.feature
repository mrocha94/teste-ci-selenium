Feature: Course edit
    In order to update courses
    As a admin
    I want to import courses from uspdigital

    Background:
        Given there is an admin user with email "kazuo@ime.usp.br" and password "admin123"

    Scenario: Admin updating courses
        Given I'm at the login page
        And there is an admin user with email "kazuo@ime.usp.br" and password "admin123"
        When I fill the "Email" field with "kazuo@ime.usp.br"
        And I fill the "Senha" field with "admin123"
        And I press the "Entrar" button

        And I click the "Painel de controle" link
        And I press the "Cadastrar disciplinas de graduação" button
        Then I should see "Disciplinas cadastradas com sucesso."