Feature: Semester edition
    In order to edit a semester
    As a secretary or admin
    I want to close a semester

    Background:
        Given there is an open semester "2014" "1"

    Scenario: Admin closing a semester
        Given I'm at the system access page
        And there is an admin user with email "kazuo@ime.usp.br" and password "admin123"
        And I click the "Login Admin" link
        Then I fill the "Email" field with "kazuo@ime.usp.br"
        And I fill the "Senha" field with "admin123"
        And I press the "Entrar" button
        And I click the "Gerenciar semestres" link
        And I click the "Fechar inscrições" link
        Then I should see "2014/1"
        And I should see "Ativo"


    Scenario: Secretary creating a semester
        Given I'm at the system access page
        And there is a secretary with name "Marcia" and password "12345678" nusp "1111111" and email "marcia@ime.usp.br"
        And I click the "Login Funcionário" link
        Then I fill the "Número USP" field with "1111111"
        And I fill the "Senha" field with "12345678"
        And I press the "Entrar" button
        And I click the "Gerenciar semestres" link
        And I click the "Fechar inscrições" link
        Then I should see "2014/1"
        And I should see "Ativo"

    Scenario: Secretary cannot edit begin and end date of a semester that is active
        Given I'm at the system access page
        And there is a secretary with name "Marcia" and password "12345678" nusp "1111111" and email "marcia@ime.usp.br"
        And I click the "Login Funcionário" link
        Then I fill the "Número USP" field with "1111111"
        And I fill the "Senha" field with "12345678"
        And I press the "Entrar" button
        And I click the "Gerenciar semestres" link
        And I click the "Editar datas" link
        Then I should see "Não é possível editar as datas enquanto o semestre esta ativo"

    Scenario: Secretary editing begin and end date of a semester that has not started yet
        Given there is a closed semester "2015" "0"
        Given there is a closed semester "2015" "1"
        And I'm at the system access page  
        And there is a secretary with name "Marcia" and password "12345678" nusp "1111111" and email "marcia@ime.usp.br"
        And I click the "Login Funcionário" link
        Then I fill the "Número USP" field with "1111111"
        And I fill the "Senha" field with "12345678"
        And I press the "Entrar" button
        And I click the "Gerenciar semestres" link
        And I click the second "Editar datas" link
        Then I should see "Incluir data de início e fim do semestre"
        Then I should see "Data de início:"
        Then I should see "Data de término:"
        And I fill the "semester_started_at" field with "2014-01-01"
        And I fill the "semester_finished_at" field with "2014-04-01"
        And I press the "Salvar" button
        Then I should see "Semestres"
        Then I should see "01/01/2014"
        Then I should see "01/04/2014"
        Then I should see "Editar datas"


