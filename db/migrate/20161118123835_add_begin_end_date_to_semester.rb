class AddBeginEndDateToSemester < ActiveRecord::Migration
  def change
  	add_column :semesters, :started_at, :datetime
    add_column :semesters, :finished_at, :datetime
    Semester.all.each do |semester|
    	semester.update(started_at: DateTime.new(semester.year, semester.months[0], 1), 
    		            finished_at:  DateTime.new(semester.year, semester.months[3], 30))
    end
    AssistantRole.all.each do |role|
      semester = role.semester
      role.update(started_at: semester.started_at, finished_at: semester.finished_at)
    end
  end
end
