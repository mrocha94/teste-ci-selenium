class Course < ActiveRecord::Base
  has_many :request_for_teaching_assistant
  belongs_to :department

  validates :department_id, presence: true
  validates :educational_level, presence: true, inclusion: { in: 0..1 }
  validates :course_code, presence: true, uniqueness: true

  def full_name
    course_code + " - " + name
  end

  def dep_code
    department.code
  end

  def self.gather_undergraduate_courses
    department = Mechanize.new
    course     = Mechanize.new
    agent      = Mechanize.new

    institute = agent.get('https://uspdigital.usp.br/jupiterweb/jupDepartamentoLista?codcg=45&tipo=D')
    departments         = institute.parser.css('table')[3].css('tr')
    departments.shift
    departments_courses = []
    courses_codes       = Hash.new(0)
    courses_names       = []
    flags               = []

    departments.each { |dep_element| departments_courses << dep_element.css('td')[1].children[1].attributes['href'].value }

    departments_courses.each do |department_link|

      if ( !department_link.match(/\=450&/) )

        # entra no departamento
        department.get("https://uspdigital.usp.br/jupiterweb/#{department_link}")

        courses = department.page.parser.css('table a')
        courses.shift
        courses.pop
        courses.pop

        # ignora a imagem formato .png da pagina
        courses.shift

        courses.each do |discpl_element|
          # discpl_element tem o nome da disciplina

          # entra no link da disciplina
          link_discpl = discpl_element.attributes['href'].value
          course.get("https://uspdigital.usp.br/jupiterweb/#{link_discpl}")

          # Entra no ultimo link da pagina pois possui o *oferecimento*
          classes_page = course.page.parser.css('table')[1].css('a').last['href']
          course.get("https://uspdigital.usp.br/jupiterweb/#{classes_page}")

          # pega o codigo da disciplina a partir do link
          course_code = link_discpl.match(/([A-Z]{3}[0-9]{4})/)
          if (course_code != nil) # Evita casos de nil

            courses_codes[course_code] += 1
            if courses_codes[course_code] == 1
              # conversao da discpl_element para uma string
              courses_names << discpl_element.to_str
            end

            # Verifica oferecimento
            if course.page.parser.css('div#web_mensagem').css('p').text.match(/Não existe oferecimento para a sigla/) == nil
              flags << true

            else flags << false # nao ha oferecimento
            end
          end

        end
      end
    end

    courses_codes.each_with_index do |course_code, index|
      department_code = course_code[0].to_s.match(/[A-Z]{3}/).to_s

      attributes = {
        educational_level: 0,
        name: courses_names[index],
        course_code: course_code[0].to_s.match(/[A-Z]{3}[0-9]{4}/).to_s,
        status: flags[index],
        department: Department.find_by(:code => (department_code))
      }
      course = Course.where(course_code: course_code[0].to_s.match(/[A-Z]{3}[0-9]{4}/).to_s)
      unless course.any?
        Course.create(attributes)
      else
        course.take.update(status: flags[index])
      end
    end
    return true
  end

  def self.gather_postgraduate_courses
    agent = Mechanize.new
    department = Mechanize.new

    # Tem que instanciar duas vezes para funcionar. Caso contrário o mechanize recebe uma página de sugestão de mudança de browser
    institute = agent.get('https://uspdigital.usp.br/janus/componente/catalogoDisciplinasInicial.jsf?action=4&tipo=D&codcpg=45')
    institute = agent.get('https://uspdigital.usp.br/janus/componente/catalogoDisciplinasInicial.jsf?action=4&tipo=D&codcpg=45')
    institute_deps = institute.iframe_with(:id => 'casca')
    departments = institute_deps.click
    departments.links.shift
    departments_courses = departments.links

    courses_codes = Hash.new(0)
    courses_names = []

    departments_courses.each do |department_link|
      department.get("https://uspdigital.usp.br/janus/#{department_link.href}")
      courses = department.page.parser.css('table[class="dataTable selecionavel"] tr')
      courses.shift
      courses.each do |discpl_element|
        course_code = discpl_element.css('td')[0].text
        courses_codes[course_code] += 1
        if courses_codes[course_code] == 1
          course_name = discpl_element.css('td')[1].text.match(/([[:alpha:][:digit:]\-,:]+ ?)+/)[0]
          courses_names << course_name

        end
      end
    end

    courses_codes.each_with_index do |course_code, index|
      department_code = course_code[0][0..2]
      attributes = {
        educational_level: 1,
        name: courses_names[index],
        course_code: course_code[0],
        department: Department.find_by(:code => (course_code[1] > 1 ? "INTERDEPARTAMENTAL" : department_code))
      }
      unless Course.where(course_code: course_code[0]).any?
        Course.create(attributes)
      end
    end
  end

end
